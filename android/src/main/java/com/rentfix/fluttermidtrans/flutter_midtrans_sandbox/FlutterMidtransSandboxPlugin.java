package com.rentfix.fluttermidtrans.flutter_midtrans_sandbox;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.UIKitCustomSetting;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.core.PaymentMethod;
import com.midtrans.sdk.corekit.models.snap.Authentication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

/**
 * FlutterMidtransSandboxPlugin
 */
public class FlutterMidtransSandboxPlugin implements FlutterPlugin, MethodCallHandler, TransactionFinishedCallback, ActivityAware, RequestPermissionsResultListener {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private FlutterPluginBinding pluginBinding;
    private ActivityPluginBinding activityBinding;
    private MethodChannel channel;
    private Context context;
    private Application application;
    private Activity activity;
    private Object initializationLock = new Object();
    static final String TAG = "FlutterMidtrans";

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final FlutterMidtransSandboxPlugin instance = new FlutterMidtransSandboxPlugin();
        Activity activity = registrar.activity();
        Application application = null;
        instance.setup(registrar.messenger(), application, activity, registrar, null);
    }

    public FlutterMidtransSandboxPlugin() {
    }

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        pluginBinding = binding;
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        pluginBinding = null;
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activityBinding = binding;
        setup(
                pluginBinding.getBinaryMessenger(),
                (Application) pluginBinding.getApplicationContext(),
                activityBinding.getActivity(),
                null,
                activityBinding);
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity();
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        onAttachedToActivity(binding);
    }

    @Override
    public void onDetachedFromActivity() {
        detach();
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("init")) {
            String clientKey = call.argument("client_key").toString();
            String baseUrl = call.argument("base_url").toString();
            String primaryColor = call.argument("primary_color").toString();
            String darkColor = call.argument("dark_color").toString();
            String secondaryColor = call.argument("secondary_color").toString();
            initMidtransSdk(clientKey, baseUrl, primaryColor, darkColor, secondaryColor);
        } else if (call.method.equals("payment")) {
            String str = call.arguments();
            payment(str);
        } else {
            result.notImplemented();
        }
    }

    private void setup(
            final BinaryMessenger messenger,
            final Application application,
            final Activity activity,
            final PluginRegistry.Registrar registrar,
            final ActivityPluginBinding activityBinding) {
        synchronized (initializationLock) {
            Log.i(TAG, "setup");
            this.activity = activity;
            this.application = application;

            // Midtrans must use activity context instead of application context.
            this.context = activity;

            channel = new MethodChannel(messenger, "flutter_midtrans_sandbox");
            channel.setMethodCallHandler(this);
            if (registrar != null) {
                // V1 embedding setup for activity listeners.
                registrar.addRequestPermissionsResultListener(this);
            } else {
                // V2 embedding setup for activity listeners.
                activityBinding.addRequestPermissionsResultListener(this);
            }
        }
    }

    private void detach() {
        Log.i(TAG, "detach");
        context = null;
        activityBinding.removeRequestPermissionsResultListener((PluginRegistry.RequestPermissionsResultListener) this);
        activityBinding = null;
        channel.setMethodCallHandler(null);
        channel = null;
    }

    private void initMidtransSdk(String client_key, String base_url, String primaryColor, String darkColor, String secondaryColor) {
        Log.d(TAG, "initMidtransSdk");
        SdkUIFlowBuilder.init()
                .setClientKey(client_key) // client_key is mandatory
                .setContext(context) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(base_url) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme(primaryColor, darkColor, secondaryColor)) // will replace theme on snap theme on MAP
                .buildSDK();
    }

    void payment(String str) {
        try {
            Log.d(TAG, str);
            JSONObject json = new JSONObject(str);
            JSONObject cJson = json.getJSONObject("customer");
            TransactionRequest transactionRequest = new
                    TransactionRequest(System.currentTimeMillis() + "", json.getInt("total"));
            ArrayList<ItemDetails> itemList = new ArrayList<>();
            JSONArray arr = json.getJSONArray("items");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                ItemDetails item = new ItemDetails(obj.getString("id"), obj.getInt("price"), obj.getInt("quantity"), obj.getString("name"));
                itemList.add(item);
            }
            CustomerDetails cus = new CustomerDetails();
            cus.setFirstName(cJson.getString("first_name"));
            cus.setLastName(cJson.getString("last_name"));
            cus.setEmail(cJson.getString("email"));
            cus.setPhone(cJson.getString("phone"));
            transactionRequest.setCustomerDetails(cus);
            boolean useCreditCard = json.has("credit_card") && json.getBoolean("credit_card");
            if (useCreditCard) {
                CreditCard creditCardOptions = new CreditCard();
                creditCardOptions.setBank(BankType.BNI);
                creditCardOptions.setAuthentication(Authentication.AUTH_3DS);
                transactionRequest.setCreditCard(creditCardOptions);
            }
            if (json.has("custom_field_1"))
                transactionRequest.setCustomField1(json.getString("custom_field_1"));
            transactionRequest.setItemDetails(itemList);
            UIKitCustomSetting setting = MidtransSDK.getInstance().getUIKitCustomSetting();
            if (json.has("skip_customer"))
                setting.setSkipCustomerDetailsPages(json.getBoolean("skip_customer"));
            MidtransSDK.getInstance().setUIKitCustomSetting(setting);
            MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
            if (useCreditCard) {
                MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.CREDIT_CARD);
            } else {
                MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BANK_TRANSFER_PERMATA);
            }
        } catch (Exception e) {
            Log.d(TAG, "ERROR " + e.getMessage());
        }
    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {
        Map<String, Object> content = new HashMap<>();
        content.put("transactionCanceled", transactionResult.isTransactionCanceled());
        content.put("status", transactionResult.getStatus());
        content.put("source", transactionResult.getSource());
        content.put("statusMessage", transactionResult.getStatusMessage());
        if (transactionResult.getResponse() != null)
            content.put("response", transactionResult.getResponse().toString());
        else
            content.put("response", null);
        channel.invokeMethod("onTransactionFinished", content);
    }

    @Override
    public boolean onRequestPermissionsResult(int i, String[] strings, int[] ints) {
        return false;
    }
}
