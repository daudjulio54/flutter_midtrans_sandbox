#import <MidtransKit/MidtransKit.h>
#import "FlutterMidtransSandboxPlugin.h"
#import "MidtransCreditCardConfig.h"

FlutterMethodChannel* channel;

@interface FlutterMidtransSandboxPayment : NSObject<MidtransUIPaymentViewControllerDelegate> {
}
@end

@implementation FlutterMidtransSandboxPayment
- (void)paymentViewController:(MidtransUIPaymentViewController *)viewController saveCard:(MidtransMaskedCreditCard *)result {
}
- (void)paymentViewController:(MidtransUIPaymentViewController *)viewController saveCardFailed:(NSError *)error {
}
- (void)paymentViewController:(MidtransUIPaymentViewController *)viewController paymentPending:(MidtransTransactionResult *)result {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
    [ret setObject:@NO forKey:@"transactionCanceled"];
    [ret setObject:@"pending" forKey:@"status"];
    [channel invokeMethod:@"onTransactionFinished" arguments:ret];
}
- (void)paymentViewController:(MidtransUIPaymentViewController *)viewController paymentSuccess:(MidtransTransactionResult *)result {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
    [ret setObject:@NO forKey:@"transactionCanceled"];
    [ret setObject:@"success" forKey:@"status"];
    [channel invokeMethod:@"onTransactionFinished" arguments:ret];
}
- (void)paymentViewController:(MidtransUIPaymentViewController *)viewController paymentFailed:(NSError *)error {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
    [ret setObject:@NO forKey:@"transactionCanceled"];
    [channel invokeMethod:@"onTransactionFinished" arguments:ret];
}
- (void)paymentViewController_paymentCanceled:(MidtransUIPaymentViewController *)viewController {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
    [ret setObject:@YES forKey:@"transactionCanceled"];
    [channel invokeMethod:@"onTransactionFinished" arguments:ret];
}
@end

@implementation FlutterMidtransSandboxPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  channel = [FlutterMethodChannel
      methodChannelWithName:@"flutter_midtrans_sandbox"
            binaryMessenger:[registrar messenger]];
  FlutterMidtransSandboxPlugin* instance = [[FlutterMidtransSandboxPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if([@"init" isEqualToString:call.method ]) {
      NSString *key = call.arguments[@"client_key"];
      NSString *url = call.arguments[@"base_url"];
      NSString *env = call.arguments[@"env"];
      MidtransServerEnvironment serverEnvironment = MidtransServerEnvironmentProduction;
      if([@"sandbox" isEqualToString:env])
          serverEnvironment = MidtransServerEnvironmentSandbox;
      [CONFIG setClientKey:key environment:serverEnvironment merchantServerURL:url];
      return result(0);
  } else if([@"payment" isEqualToString:call.method]) {
      NSString *str = call.arguments;
      id delegate = [FlutterMidtransSandboxPayment alloc];
      NSError *error = nil;
      id object = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:true] options:0 error:&error];
      if([object isKindOfClass:[NSDictionary class]]) {
          NSDictionary *json = object;
          NSDictionary *customer = json[@"customer"];
          CFAbsoluteTime timeInSeconds = CFAbsoluteTimeGetCurrent();

          MidtransAddress *address = [MidtransAddress addressWithFirstName:@"flutter_midtrans_sandbox" lastName:@"flutter_midtrans_sandbox" phone:@"081" address:@"address" city:@"city" postalCode:@"55181" countryCode:@"id"];
          MidtransCustomerDetails *custDetail = [[MidtransCustomerDetails alloc] initWithFirstName:customer[@"first_name"] lastName: customer[@"last_name"] email: customer[@"email"] phone: customer[@"phone"] shippingAddress:address billingAddress:address];
          NSMutableArray *arr = [NSMutableArray new];
          NSArray *items = json[@"items"];
          NSString *orderId;
          for(int i = 0; i < [items count]; i++) {
              NSDictionary *itemJson = items[i];
              MidtransItemDetail *item = [MidtransItemDetail alloc];
              [item initWithItemID:itemJson[@"id"] name:itemJson[@"name"] price: itemJson[@"price"] quantity:itemJson[@"quantity"]];
              [arr addObject:item];
              orderId = itemJson[@"id"];
          }

          NSNumber* total = json[@"total"];
          MidtransTransactionDetails *transDetail = [MidtransTransactionDetails alloc];
                    [transDetail initWithOrderID:orderId andGrossAmount:total];

          NSMutableArray *arrayOfCustomField = [NSMutableArray new];
          [arrayOfCustomField addObject:@{MIDTRANS_CUSTOMFIELD_1:json[@"custom_field_1"]}];
          [arrayOfCustomField addObject:@{MIDTRANS_CUSTOMFIELD_2:@""}];
          [arrayOfCustomField addObject:@{MIDTRANS_CUSTOMFIELD_3:@""}];
          [[MidtransMerchantClient shared] requestTransactionTokenWithTransactionDetails:transDetail itemDetails:arr customerDetails:custDetail customField:arrayOfCustomField binFilter:nil blacklistBinFilter:nil transactionExpireTime:nil completion:^(MidtransTransactionTokenResponse *token, NSError *error)
           {

               BOOL useCreditCard = [[json valueForKey:@"credit_card"] boolValue];
               NSLog(@"INIII CREDIT CARD: || %i ||",useCreditCard);

               if (token) {
                   MidtransUIPaymentViewController *vc;
                   if(useCreditCard){
//                       CC_CONFIG.paymentType = MTCreditCardPaymentTypeTwoclick;
//                       CC_CONFIG.saveCardEnabled = YES;

                       CC_CONFIG.paymentType = MTCreditCardPaymentTypeOneclick;
                       //CC_CONFIG.saveCardEnabled = YES;
                       //1-click need token storage enabled
                       //CC_CONFIG.tokenStorageEnabled = YES;
                       //1-click need 3ds enabled
                       CC_CONFIG.secure3DEnabled = YES;
                       CC_CONFIG.acquiringBank = MTAcquiringBankBNI;

                       vc = [[MidtransUIPaymentViewController alloc] initWithToken:token andPaymentFeature:MidtransPaymentFeatureCreditCard];
                   } else {
                       NSLog(@"INIII CREDIT CARD: FALSEEE");
                       vc = [[MidtransUIPaymentViewController alloc] initWithToken:token andPaymentFeature:MidtransPaymentFeatureBankTransferPermataVA];
//                       vc = [[MidtransUIPaymentViewController new] initWithToken:token];
                   }




                   vc.paymentDelegate = delegate;
                   UIViewController *viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                   [viewController presentViewController:vc animated:YES completion:nil];
               }
           }];
          return result(0);
      }
  } else {
    result(FlutterMethodNotImplemented);
  }
}
@end
