#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_midtrans_sandbox.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_midtrans_sandbox'
  s.version          = '0.0.1'
  s.summary          = 'Flutter Sandbox Midtrans Payment Gateway'
  s.description      = <<-DESC
Flutter Sandbox Midtrans Payment Gateway
                       DESC
  s.homepage         = 'https://rentfix.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'PT Real Estate Teknologi' => 'daudjulio54@gmail.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'MidtransCoreKit'
  s.dependency 'MidtransKit'
  s.platform = :ios, '9.0'
  s.static_framework = true

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end
