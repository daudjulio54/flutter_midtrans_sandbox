import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_midtrans_sandbox/flutter_midtrans_sandbox.dart';

void main() {
  const MethodChannel channel = MethodChannel('flutter_midtrans_sandbox');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await FlutterMidtransSandbox.platformVersion, '42');
  });
}
